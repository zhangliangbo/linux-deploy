package xxl.netlogo.netlogo.bean


import javax.persistence.*

@Entity
class ClassRoom {
    @Id
    @GeneratedValue
    long id

    @Basic
    String name

//    @ManyToOne(targetEntity = Student.class)
//    @SortNatural
//    SortedSet<Student> students
//
//    @ManyToMany(targetEntity = Teacher.class)
//    @SortNatural
//    SortedSet<Teacher> teachers

    @ManyToMany(mappedBy = 'classrooms')
    List<Student> student

    @OneToOne()
    Teacher teacher
}
