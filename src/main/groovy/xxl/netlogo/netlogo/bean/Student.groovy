package xxl.netlogo.netlogo.bean

import javax.persistence.*

@Entity
class Student {
    @Id
    @GeneratedValue
    long id

    @Basic
    String name

    @ManyToMany()
    List<ClassRoom> classrooms
}
