package xxl.netlogo.netlogo.bean


import javax.persistence.Basic
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
class Teacher {

    @Id
    @GeneratedValue
    long id

    @Basic
    String name

}
