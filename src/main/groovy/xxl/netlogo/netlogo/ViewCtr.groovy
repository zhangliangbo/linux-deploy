package xxl.netlogo.netlogo

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping('/')
class ViewCtr {

    @GetMapping('/{path}')
    String route(@PathVariable String path, Model model) {
        return path
    }
}
