package xxl.netlogo.netlogo

import io.reactivex.Completable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import org.apache.commons.io.IOUtils
import org.apache.commons.lang3.SystemUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import xxl.mathematica.external.External

@RestController
@RequestMapping('/external')
class ExternalCtr {

    @Value('${server.port}')
    String selfPort

    @PostMapping('/file')
    String uploadJar(@RequestParam String port, @RequestParam MultipartFile jar) {
        if (selfPort == port) {
            return '不能重新部署自己'
        }
        if (jar == null || jar.getBytes().length == 0) {
            String process = new String(External.runProcess('lsof -i:' + port))
            if (process != null && !process.isEmpty()) {
                String[] lines = process.split('\\n')
                for (def line in lines) {
                    String[] items = line.split('\\s+')
                    if (items.length > 1) {
                        if (items[0] == 'java') {
                            //杀死旧的程序
                            External.runProcess('kill -9 ' + items[1])
                            return '杀死旧程序' + items[1] + '，无jar包，无法重新部署'
                        } else {
                            return '禁止杀死除java以外的程序'
                        }
                    }
                }
            }
            return '无旧程序，无jar包，无法重新部署'
        }
        def dir = new File(SystemUtils.getUserDir().getAbsolutePath() + File.separator + 'file')
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                return '创建文件夹失败'
            }
        }
        File dest = new File(dir.getAbsolutePath() + File.separator + jar.getOriginalFilename())
        if (dest.exists() && dest.isFile()) {
            if (!dest.delete()) {
                return '删除旧文件失败'
            }
        }
        def fos = new FileOutputStream(dest)
        IOUtils.copyLarge(jar.getInputStream(), fos)
        IOUtils.closeQuietly(jar.getInputStream(), fos)
        String process = new String(External.runProcess('lsof -i:' + port))
        if (process != null && !process.isEmpty()) {
            String[] lines = process.split('\\n')
            for (def line in lines) {
                String[] items = lines[1].split('\\s+')
                if (items.length > 1) {
                    if (items[0] == 'java') {
                        //杀死旧的程序
                        External.runProcess('kill -9 ' + items[1])
                        //重新部署
                        Completable.fromAction(new Action() {
                            @Override
                            void run() throws Exception {
                                External.runProcess(dir, 'nohup java -server -jar ' + jar.getOriginalFilename() + ' &')
                            }
                        }).subscribeOn(Schedulers.newThread()).subscribe()
                        return '杀死旧进程' + items[1] + '，并重新部署->' + process
                    } else {
                        return '禁止杀死除java以外的程序'
                    }
                }
            }
        } else {
            Completable.fromAction(new Action() {
                @Override
                void run() throws Exception {
                    External.runProcess(dir, 'nohup java -server -jar ' + jar.getOriginalFilename() + ' &')
                }
            }).subscribeOn(Schedulers.newThread()).subscribe()
            return '无旧程序，直接重新部署->' + process
        }
    }

    @PostMapping
    String runProcess(@RequestParam String command) {
        return new String(External.runProcess(command), 'GBK')
    }

}
