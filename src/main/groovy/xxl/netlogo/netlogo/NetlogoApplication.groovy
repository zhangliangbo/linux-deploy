package xxl.netlogo.netlogo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class NetlogoApplication {

    static void main(String[] args) {
        SpringApplication.run(NetlogoApplication, args)
    }

}
